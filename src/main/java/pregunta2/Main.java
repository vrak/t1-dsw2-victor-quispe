package pregunta2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String pathJSON= System.getProperty("user.dir") + "\\src\\main\\resources\\Cuenta.json";
		String pathFolder= System.getProperty("user.dir") + "\\src\\main\\resources\\";
		String jsonContent = new String(Files.readAllBytes(Paths.get(pathJSON)));
		JSONArray jsonArray = new JSONArray(jsonContent);
		int cuentaAceptada = 1;
		int cuentaRechazada = 1;
		
		try {
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Boolean estado = jsonObject.getBoolean("estado");
				int nroCuenta = jsonObject.getInt("nro_cuenta");
				double saldo = jsonObject.getDouble("saldo");
				String banco = jsonObject.getString("banco");
				
				if(estado == true && saldo > 5000) {
					String nameFile = "Cuentas Aceptadas" + " " + cuentaAceptada + ".txt";
					String fullPath = pathFolder + nameFile.trim();
					
					File f = new File(fullPath);
							
					if(!f.exists()) {
						FileWriter writer = new FileWriter(fullPath);
						writer.write("Banco de origen: " + banco+ "\n" );
						writer.write("La cuenta con el nro de cuenta: " + nroCuenta + " tiene un saldo de: " + saldo + "\n");
						writer.write("Usted es apto a participar en el  concurso de la SBS por 10000.00 soles. \nSuerte!");
						writer.close();
						cuentaAceptada++;
						System.out.println("Archivo creado exitosamente!!");
					} else if (f.exists()){
						System.out.println("Elimine el archivo " + nameFile + " puesto que ya existe");
					}
						
				} else if (estado == true && saldo < 5000){
					String fileName = "Cuentas Rechazadas" + " " + cuentaRechazada + ".txt";
					String fullPath = pathFolder + fileName.trim();
					
					File f = new File(fullPath);
					
					if(!f.exists()) {
						FileWriter writer = new FileWriter(fullPath);
						writer.write("Banco de origen: " + banco + "\n");
						writer.write("La cuenta con el nro de cuenta: " + nroCuenta + " tiene un saldo de: " + saldo + "\n");
						writer.write("Lamentablemente no podrá acceder al concurso de la SBS por 10000.00 soles.\nGracias");
						writer.close();
						cuentaRechazada++;
						System.out.println("Archivo creado exitosamente!!");
					} else {
						System.out.println("Elimine el archivo " + fileName + " puesto que ya existe");
					}
					
					
				} else {
					
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cuentaAceptada = 1;
			cuentaRechazada = 1;
		}
	}

}
