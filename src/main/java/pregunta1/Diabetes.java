package pregunta1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Diabetes implements Runnable {
	
	public int numElementos;
	public String numArray;
	public int indice;
 
	public Diabetes(String numArray ,int numElementos) {
		this.numArray = numArray;
		this.numElementos = numElementos;
		this.indice = 0;
	}
	/*
	public void llenarElementos() {
		array.add(1);
		array.add(2);
		array.add(3);
		array.add(4);
		array.add(5);
		array.add(6);
		array.add(7);
		array.add(8);
		array.add(9);
		array.add(10);
		indice = 10;
		
		int numero = generarElementoArray();
		numeritos = new ArrayList<Integer>();
		numeritos.add(numero);
		indice++;
		printElementos();
		
		for(int i = 0; i < numElementos; i++) {
			int generationX = generarElementoArray();
			array.add(1);
			array.add(2);
			array.add(3);
			indice += i;
			printElementos();
		}
	}
	*/
	
	public Integer[] append(Integer[] arr, int element) {
		List<Integer> list = new ArrayList<>(Arrays.asList(arr));
        list.add(element);
 
        return list.toArray(new Integer[0]);
	}
	
	public int generarElementoArray() {
		int max = 200;
		int min = 70;
		return (int) Math.floor(Math.random() * (max - min + 1) + min);
	}
	
	public boolean hasFinish() {
		return indice >= numElementos; 
	}
	
	public void printElementos() {
		System.out.println(numArray + " se esta llenando " /*+ array.get(0)*/);
	}
	
	public Integer[] clasificacion(Integer[] arr) {
		List<Integer> list = new ArrayList<>(Arrays.asList(arr));
		return list.toArray(new Integer[0]);
		
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			Integer[] nums = {};
			while (!hasFinish()) {
				int numeroRandom = generarElementoArray();
				nums = append(nums, numeroRandom);
				indice++;
				Thread.sleep(100);
			}
			//int dato = generarElementoArray();
			System.out.println("El array " + numArray + " " + Arrays.toString(nums) + " ha terminado de crearse");

		} catch (InterruptedException e) {
			System.out.println("Generación de resultado detenido");
		}
	}

}
