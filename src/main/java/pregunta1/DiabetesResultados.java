package pregunta1;

public class DiabetesResultados {
	
	public Diabetes[] diabetes;
	public Thread[] hilos;
	
	public DiabetesResultados(int numArrays) throws InterruptedException {
		diabetes = new Diabetes[numArrays];
		hilos = new Thread[numArrays];
		for (int i = 0; i < numArrays; i++) {
			diabetes[i] = new Diabetes("Array " + i, 10);
			System.out.println("Array " + i + " generado correctamente");
		}
		System.out.println("\nGenerando... 10%");
		Thread.sleep(300);
		System.out.println("Cargando... 20%");
		Thread.sleep(300);
		System.out.println("Cargando... 30%");
		Thread.sleep(300);
		System.out.println("Cargando... 40%");
		Thread.sleep(300);
		System.out.println("Cargando... 50%");
		Thread.sleep(300);
		System.out.println("Cargando... 60%");
		Thread.sleep(300);
		System.out.println("Cargando... 70%");
		Thread.sleep(300);
		System.out.println("Cargando... 80%");
		Thread.sleep(300);
		System.out.println("Cargando... 90%");
		Thread.sleep(300);
		System.out.println("Finalizando...\n");
		Thread.sleep(300);
	}
	
	public void empezarGeneracion() {
		for (int i =0; i < diabetes.length; i++) {
			hilos[i] = new Thread(diabetes[i]);
			hilos[i].start();
		}
	}
}
